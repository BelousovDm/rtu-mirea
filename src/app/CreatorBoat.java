package app;

public class CreatorBoat extends Creator {

    @Override
    public Transport factoryMethod() {
        return new Boat();
    }
}
