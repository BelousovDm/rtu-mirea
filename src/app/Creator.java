package app;

abstract class Creator {

    abstract public Transport factoryMethod();

    public void delivery() {
        Transport transport = factoryMethod();
        transport.delivery();
    }

}
