package app;

public class CreatorCar extends Creator{
    @Override
    public Transport factoryMethod() {
        return new Car();
    }
}
